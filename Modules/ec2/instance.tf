resource "aws_instance" "web" {
  ami           = var.ami
  instance_type = var.instance_type
  subnet_id = var.subnetID
  count = var.ec2_count
  tags = {
    Name = var.Instance_name
  }
}